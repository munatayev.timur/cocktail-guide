package app.snappik.cocktailguide.viewmodel

import android.graphics.Bitmap
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.model.Resource
import app.snappik.cocktailguide.repository.Repository
import app.snappik.cocktailguide.storage.LocalStorage
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CocktailsViewModel @Inject constructor(
    private val repository: Repository,
    private val remoteStorage: FirebaseStorage,
    private val localStorage: LocalStorage
) : AbstractViewModel() {

    private val mCocktails = MutableStateFlow<Resource<Set<Cocktail>>>(Resource.Loading())
    val cocktails: StateFlow<Resource<Set<Cocktail>>> = mCocktails

    private val mFavCocktails = MutableStateFlow<Resource<Set<Cocktail>>>(Resource.Loading())
    val favorites: StateFlow<Resource<Set<Cocktail>>> = mFavCocktails

    private val mRandomCocktailsLocal = MutableStateFlow<Resource<Set<Cocktail>>>(Resource.Loading())
    val randomCocktailsLocal: StateFlow<Resource<Set<Cocktail>>> = mRandomCocktailsLocal

    private val mRandomCocktailsRemote = MutableStateFlow<MutableList<Resource<Cocktail>>>(
        MutableList(AMOUNT_RANDOM) {
        val id = (1..MAX_ELEMENTS).random()
        val cocktail = Cocktail(id = id.toLong())
        Resource.Loading(cocktail)
    })
    val randomCocktailsRemote: StateFlow<List<Resource<Cocktail>>> = mRandomCocktailsRemote

    private val mRecentlyOpened = MutableStateFlow<Resource<Set<Cocktail>>>(Resource.Loading())
    val recentlyOpened: StateFlow<Resource<Set<Cocktail>>> = mRecentlyOpened

    //amount of items which requested from repository(remote and local)
    private var loadedAmount = 0L

    private var getCocktailsJob: Job? = null

    init {
        initSearchItems()
        initFavoriteItems()
        initLocalSuggestions()
        initRecentlyOpened()
    }

    private fun initSearchItems(){
        getCocktailsJob = dbScope.launch {
            repository.getCocktails(INITIAL_FIRST_ITEM_ID, INITIAL_LAST_ITEM_ID).collect {
                mCocktails.value = it

                //Close listener, only favorites could change data
                if(it is Resource.Success) {
                    loadedAmount += it.data?.size ?: 0
                    getCocktailsJob?.cancel()
                }
            }
        }
    }

    private fun initFavoriteItems(){
        dbScope.launch {
            repository.getFavorites()
                .collect { favorites ->
                    mFavCocktails.value = favorites
                }
        }
    }

    private fun initLocalSuggestions(){
        dbScope.launch {
            repository.getRandomCocktails(AMOUNT_RANDOM)
                .collect {
                    mRandomCocktailsLocal.value = it
                }
        }
    }

    private fun initRecentlyOpened(){
        dbScope.launch {
            repository.getRecentlyOpened()
                .collect {
                    mRecentlyOpened.value = it
                }
        }
    }

    @ExperimentalCoroutinesApi
    fun loadRandom(){
        dbScope.launch {
            //1.Generate ids which we want to show
            mRandomCocktailsRemote.value
                .map { randomId ->
                    //2.Check if we have element with such id in search list
                    cocktails.value.data
                        ?.firstOrNull { it.id == randomId.data?.id }
                        ?.let { cocktail ->  flowOf(Resource.Success(cocktail)) }
                        //3. If there is no such item in search list get from local or then remote
                        ?: repository.getById(randomId.data?.id ?: 0)
                }.merge()
                .collect { res ->
                    //4. some items could be on local and some could be on remote, each items loads differently
                    mRandomCocktailsRemote.value.firstOrNull {
                        it.data?.id == res.data?.id
                    }?.let {
                        //5.In case of success put data (Loading -> Success)
                        val indexOfItem = mRandomCocktailsRemote.value.indexOf(it)
                        if(indexOfItem >= 0) {
                            mRandomCocktailsRemote.value[indexOfItem] = res
                        }
                    }
                }
        }
    }

    fun getById(id: Long) = (mCocktails.value.data
        //look at the existing list
        ?.firstOrNull { cocktail -> cocktail.id == id }
        //if such cocktail already loaded return
        ?.let { cocktail -> flowOf(Resource.Success(cocktail)) }
        //if not found get from database or remote
        ?: repository.getById(id)).onCompletion {
            dbScope.launch {
                repository.updateOpenDate(id)
            }
    }

    fun updateFavoriteStatus(cocktailId: Long, isFavorite: Boolean) = dbScope.launch {

        //Update favorite status, this change will not fire subscription on cocktails
        mCocktails.value = mCocktails.value.apply {
            data?.firstOrNull { it.id == cocktailId }?.isFavorite = isFavorite
        }

        //Update data on database, this change will file collect for favorites
        repository.updateFavoriteStatus(cocktailId, isFavorite)
    }

    fun search(filter: String){
        getCocktailsJob?.cancel()
        getCocktailsJob = dbScope.launch {
            repository.search(filter).collect {

                //show result of search
                mCocktails.value = it

                //prepare amount that loaded to fetch from the beginning after filter cleared
                loadedAmount = 0

                if(it is Resource.Success) {
                    getCocktailsJob?.cancel()
                }
            }
        }
    }

    fun loadMore() {

        //Close job to avoid calling collect at line @see 29
        getCocktailsJob?.cancel()
        getCocktailsJob = dbScope.launch {

            //when zero items we should start from 0
            val fromItem = if(loadedAmount == 0L) 0 else loadedAmount + 1

            //Fetch next piece of data (+20) and send to all subscribers
            repository.getCocktails(fromItem, loadedAmount + INITIAL_LAST_ITEM_ID).collect {

                //Notify data changed (if cleared filter)
                mCocktails.value = if(loadedAmount == 0L) it else it.apply {
                    data = setOf(mCocktails.value.data ?: emptySet(), data ?: emptySet()).flatten().toSet()
                }

                //If loaded 20-30 here, so changes on 1-20 will bot be visible (here triggers only 20-30)
                if(it is Resource.Success) {
                    loadedAmount = it.data?.maxByOrNull { cocktail -> cocktail.id }?.id ?: 0
                    getCocktailsJob?.cancel()
                }
            }
        }
    }

    fun getImage(name: String?): MutableStateFlow<Resource<Bitmap>>{
        val result = MutableStateFlow<Resource<Bitmap>>(Resource.Loading())
        if(name.isNullOrEmpty()) return result
        dbScope.launch {
            //Get image from local storage
            localStorage.getImage(name)
                .onStart {
                    result.value = Resource.Loading()
                }
                //If image exist in local storage it is success
                .transform<Bitmap, Resource<Bitmap>> { value ->
                    emit(Resource.Success(value))
                }
                //If there is no saved locally image try to get from remote storage
                .catch {
                    emit(localStorage.fetch(remoteStorage.reference.child(name)))
                }
                //Assign result
                .collect {
                    result.value = it
                }
        }
        return result
    }

    companion object {
        private const val MAX_ELEMENTS = 548
        private const val INITIAL_FIRST_ITEM_ID = 1L
        const val INITIAL_LAST_ITEM_ID = 20L
        private const val AMOUNT_RANDOM = 10
    }
}