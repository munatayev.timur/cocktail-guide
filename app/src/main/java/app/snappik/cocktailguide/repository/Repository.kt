package app.snappik.cocktailguide.repository

import app.snappik.cocktailguide.database.dao.CocktailDao
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.model.Resource
import com.google.firebase.database.DatabaseReference
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class Repository @Inject constructor(
    private val cocktailDao: CocktailDao,
    private val remoteDao: DatabaseReference
) {

    suspend fun getCocktails(from: Long, to: Long) = flow<Resource<Set<Cocktail>>> {
        cocktailDao.getRange(from, to)
            //1.Show loader status
            .onStart { emit(Resource.Loading()) }
            //2.Collect data from database
            .collect { list ->
                list.takeIf { it.isNotEmpty() }
                    //2.If data from database is not empty return SuccessResult(with data from database)
                    ?.let { emit(Resource.Success(it.toSet())) }
                    //3.If data from database is empty fetch from service
                    ?: emit(fetch(from, to))
            }
    }

    suspend fun search(filter: String) = flow<Resource<Set<Cocktail>>> {
        cocktailDao.search("%$filter%")
            //1.Show loader status
            .onStart { emit(Resource.Loading()) }
            //2.Collect data from database
            .collect { list ->
                //3. Always try to get data from remote to make sure that all data covered
                emit(fetch(filter, list))
            }
    }

    //Compare remote and local data because (Case: 1 saved locally from pagination from 3)
    private suspend fun fetch(filter: String, localResult: List<Cocktail>) = suspendCoroutine<Resource<Set<Cocktail>>> { cont ->
        remoteDao
            .orderByChild("name")
            .startAt(filter)
            .endAt(filter+"\uf8ff")
            .get()
            .addOnCanceledListener {
                //if offline or network problems return local
                if(localResult.isEmpty()) {
                    cont.resume(Resource.Error("Firebase fetch canceled"))
                } else cont.resume(Resource.Success(localResult.toSet()))
            }
            .addOnFailureListener {
                //if offline or network problems return local
                if(localResult.isEmpty()) {
                    cont.resume(Resource.Error(it.message ?: "Firebase fetch failed"))
                } else cont.resume(Resource.Success(localResult.toSet()))
            }
            .addOnSuccessListener { dataSnapshot ->
                CoroutineScope(Dispatchers.IO).launch {
                    dataSnapshot
                        .children

                        //5.Map downloaded data
                        .mapNotNull {
                            it.getSaveValue(Cocktail::class.java)

                                //need to store localId = remoteId (data downloaded from filters should be in strong order)
                                ?.apply { id = it.key?.toLong() ?: 0L }
                        }

                        // if remote data is bigger than local, save the difference
                        .takeIf { remoteData -> remoteData.size > localResult.size }

                        //Remote data is bigger than local
                        ?.let {
                            //6. Save data to database (if something saved collect will be triggered)
                            val somethingSaved = saveItems(it)
                            //7. Emit to flow, which fires collect again with element added to database @see 26
                            if (somethingSaved.not()) cont.resume(Resource.Success(it.toSet()))

                            //return local data because everything is persisted
                        } ?: cont.resume(Resource.Success(localResult.toSet()))
                }
            }
    }

    //4.Making from callback -> suspend function + no need filter because we want to download data until the screen will be filled
    private suspend fun fetch(from: Long, to: Long) = suspendCoroutine<Resource<Set<Cocktail>>> { cont ->
        remoteDao
            .orderByKey()
            .startAt(from.toString())
            .limitToFirst(to.toInt() - from.toInt())
            .get()
            .addOnCanceledListener { cont.resume(Resource.Error("Firebase fetch canceled")) }
            .addOnFailureListener { cont.resume(Resource.Error(it.message ?: "Firebase fetch failed")) }
            .addOnSuccessListener { dataSnapshot ->
                CoroutineScope(Dispatchers.IO).launch {
                    dataSnapshot
                        .children

                        //5.Map downloaded data
                        .mapNotNull {
                            it.getSaveValue(Cocktail::class.java)

                                //need to store localId = remoteId (data downloaded from filters should be in strong order)
                                ?.apply { id = it.key?.toLong() ?: 0L }
                        }
                        .also {
                            //6. Save data to database (if something saved collect will be triggered)
                            val somethingSaved = saveItems(it)
                            //7. Emit to flow, which fires collect again with element added to database @see 26
                            if (somethingSaved.not()) cont.resume(Resource.Success(it.toSet()))
                        }
                }
            }
    }

    //Return true if some elements saved and false if all data already saved locally
    private suspend fun saveItems(list: List<Cocktail>) =
        list.mapNotNull { cocktail -> if(cocktailDao.checkExists(cocktail.id).not()) cocktail else null }
            .also { notSavedCocktails -> cocktailDao.insertAll(notSavedCocktails) }
            .isNotEmpty()

    suspend fun updateFavoriteStatus(cocktailId: Long, isFavorite: Boolean) = cocktailDao.changeFavorite(cocktailId, isFavorite)

    suspend fun getRandomCocktails(amount: Int) = flow<Resource<Set<Cocktail>>> {
        cocktailDao.getRandomCocktails(amount)
            .onStart { emit(Resource.Loading()) }
            .collect { favorites -> emit(Resource.Success(favorites.toSet())) }
    }

    suspend fun getFavorites() = flow<Resource<Set<Cocktail>>> {
        cocktailDao.getFavorites()
            .onStart { emit(Resource.Loading()) }
            .collect { favorites -> emit(Resource.Success(favorites.toSet())) }
    }

    suspend fun getRecentlyOpened() = flow<Resource<Set<Cocktail>>> {
        cocktailDao.getRecentlyOpened(10)
            .onStart { emit(Resource.Loading()) }
            .collect { lasOpened -> emit(Resource.Success(lasOpened.toSet())) }
    }

    suspend fun updateOpenDate(cocktailId: Long) = cocktailDao.updateOpenDate(cocktailId, System.currentTimeMillis())

    fun getById(id: Long) = flow {
        cocktailDao.getById(id)
            //1.Show loader status
            .onStart { emit(Resource.Loading(Cocktail().apply { this.id = id })) }
            //2.Collect data from database
            .collect { cocktail ->
                //3. Already have such cocktail
                if(cocktail != null){
                    emit(Resource.Success(cocktail))
                } else {
                    //4.fetch from remote
                    emit(fetchById(id))
                }
            }
    }

    private suspend fun fetchById(id: Long)= suspendCoroutine<Resource<Cocktail>> { cont ->
        remoteDao
            .orderByKey()
            .equalTo(id.toString())
            .get()
            .addOnCanceledListener { cont.resume(Resource.Error("Firebase fetch canceled")) }
            .addOnFailureListener { cont.resume(Resource.Error(it.message ?: "Firebase fetch failed")) }
            .addOnSuccessListener { dataSnapshot ->
                CoroutineScope(Dispatchers.IO).launch {
                    dataSnapshot.children
                        .map { it.getSaveValue(Cocktail::class.java) }
                        .firstOrNull()
                        ?.apply { this.id = id }
                        ?.also { cocktail ->
                            saveItems(listOf(cocktail))
                            cont.resume(Resource.Success(cocktail))
                        } ?: cont.resume(Resource.Error("Firebase data can not to be parsed"))
                }
            }
    }
}