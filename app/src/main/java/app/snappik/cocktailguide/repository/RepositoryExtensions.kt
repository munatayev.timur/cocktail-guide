package app.snappik.cocktailguide.repository

import com.google.firebase.database.DataSnapshot

fun <T> DataSnapshot.getSaveValue(jClass: Class<T>): T?{
    return try { getValue(jClass) } catch (e: Exception){ null }
}