package app.snappik.cocktailguide.database.converters

import androidx.room.TypeConverter
import app.snappik.cocktailguide.model.Ingredient
import com.google.gson.Gson


class IngredientsConverter {

    @TypeConverter
    fun fromIngredients(list: List<Ingredient>) = Gson().toJson(list)

    @TypeConverter
    fun toIngredients(value: String) =
        Gson().fromJson(value, Array<Ingredient>::class.java).toList()
}