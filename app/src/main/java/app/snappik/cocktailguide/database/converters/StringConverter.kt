package app.snappik.cocktailguide.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson

class StringConverter {
    @TypeConverter
    fun fromStrings(list: List<String>) = Gson().toJson(list)

    @TypeConverter
    fun toStrings(value: String) =
        Gson().fromJson(value, Array<String>::class.java).toList()
}