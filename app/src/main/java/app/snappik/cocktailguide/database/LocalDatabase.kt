package app.snappik.cocktailguide.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import app.snappik.cocktailguide.database.converters.IngredientsConverter
import app.snappik.cocktailguide.database.converters.StringConverter
import app.snappik.cocktailguide.database.dao.CocktailDao
import app.snappik.cocktailguide.model.Cocktail

@Database(entities = [Cocktail::class], version = 1, exportSchema = false)
@TypeConverters(value = [IngredientsConverter::class, StringConverter::class])
abstract class LocalDatabase : RoomDatabase() {
    abstract val cocktailDao:CocktailDao

    companion object{

        private const val DATABASE_NAME = "cocktail_guid.db"

        @Volatile private var instance: LocalDatabase? = null

        fun getDatabase(context: Context): LocalDatabase =
            instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it } }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, LocalDatabase::class.java, DATABASE_NAME).build()
    }
}