package app.snappik.cocktailguide.database.dao

import androidx.room.*
import app.snappik.cocktailguide.model.Cocktail
import kotlinx.coroutines.flow.Flow

@Dao
interface CocktailDao {

    @Query("SELECT * FROM cocktail WHERE id = :id LIMIT 1")
    fun getById(id: Long): Flow<Cocktail?>

    @Query("SELECT EXISTS(SELECT * FROM cocktail WHERE id = :id)")
    suspend fun checkExists(id: Long): Boolean

    @Query("SELECT * FROM cocktail WHERE isFavorite = 1")
    fun getFavorites(): Flow<List<Cocktail>>

    @Query("SELECT * FROM cocktail WHERE id BETWEEN :from AND :to")
    fun getRange(from: Long, to: Long): Flow<List<Cocktail>>

    @Query("SELECT * FROM cocktail WHERE name LIKE :filter")
    fun search(filter: String): Flow<List<Cocktail>>

    @Query("SELECT * FROM cocktail ORDER BY RANDOM() LIMIT :amount")
    fun getRandomCocktails(amount: Int): Flow<List<Cocktail>>

    @Query("SELECT COUNT(*) FROM cocktail")
    suspend fun count(): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(cocktails: List<Cocktail>)

    @Query("UPDATE cocktail SET isFavorite = :isFavorite WHERE id = :cocktailId")
    suspend fun changeFavorite(cocktailId: Long, isFavorite: Boolean)

    @Query("SELECT * FROM cocktail WHERE openedDate > 0 ORDER BY openedDate DESC LIMIT :amount")
    fun getRecentlyOpened(amount: Int): Flow<List<Cocktail>>

    @Query("UPDATE cocktail SET openedDate = :date WHERE id = :cocktailId")
    suspend fun updateOpenDate(cocktailId: Long, date: Long)
}