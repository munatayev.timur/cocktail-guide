package app.snappik.cocktailguide.di

import android.content.Context
import androidx.room.Room
import app.snappik.cocktailguide.database.LocalDatabase
import app.snappik.cocktailguide.database.dao.CocktailDao
import app.snappik.cocktailguide.repository.Repository
import app.snappik.cocktailguide.storage.LocalStorage
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModel {

    private const val DATABASE_NAME = "cocktail_guid.db"
    private const val REMOTE_DATABASE_URL = "https://cocktail-guide-55dcc-default-rtdb.europe-west1.firebasedatabase.app"

    @Singleton
    @Provides
    fun provideLocalDatabase(@ApplicationContext appContext: Context) =
        Room.databaseBuilder(appContext, LocalDatabase::class.java, DATABASE_NAME).build()

    @Singleton
    @Provides
    fun provideRemoteDatabase() =
        FirebaseDatabase.getInstance(REMOTE_DATABASE_URL)

    @Singleton
    @Provides
    fun provideRemoteStorage() = Firebase.storage

    @Singleton
    @Provides
    fun provideLocalStorage(@ApplicationContext appContext: Context) = LocalStorage(appContext)

    @Singleton
    @Provides
    fun provideCocktailDao(db: LocalDatabase) = db.cocktailDao

    @Singleton
    @Provides
    fun provideRepository(cocktailDao: CocktailDao,
                          remoteDatabase: FirebaseDatabase) =
        Repository(cocktailDao, remoteDatabase.reference)

}