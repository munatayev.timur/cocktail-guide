package app.snappik.cocktailguide.model

import java.io.Serializable

data class Ingredient(
    val ingredient: String = "",
    val measure: String = "",
    val quantity: String = ""
) : Serializable{

    override fun toString(): String = "${ingredient},${measure},${quantity}"

    companion object {
        fun toIngredient(ingredient: String): Ingredient {
            val list = ingredient.split(",")
            return Ingredient(list[1], list[2], list[3])
        }
    }
}