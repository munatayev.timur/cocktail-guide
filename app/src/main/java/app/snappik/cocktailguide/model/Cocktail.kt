package app.snappik.cocktailguide.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "cocktail")
data class Cocktail(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    val directions: List<String> = emptyList(),
    val description: String = "",
    val image: String = "",
    val ingredients: List<Ingredient> = emptyList(),
    val keywords: List<String> = emptyList(),
    val name: String = "",
    var isFavorite: Boolean = false,
    var openedDate: Long = 0
) : Serializable{
    fun getPreparedIngredients() = ingredients.joinToString("\n\n") { "* ${it.quantity} ${it.ingredient} ${it.measure}" }
    fun getSteps() = directions.mapIndexed { index, text -> "${index + 1}. $text"  }.joinToString("\n\n")
}