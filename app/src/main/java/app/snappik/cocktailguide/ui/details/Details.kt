package app.snappik.cocktailguide.ui.details

import android.graphics.Bitmap
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.model.Resource
import app.snappik.cocktailguide.ui.components.Image
import app.snappik.cocktailguide.ui.theme.CocktailsTheme
import app.snappik.cocktailguide.viewmodel.CocktailsViewModel
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.insets.statusBarsPadding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import java.lang.Float.min

@ExperimentalCoroutinesApi
@Composable
fun Details(
    cocktailId: Long,
    cocktailsViewModel: CocktailsViewModel
){
    val cocktail = produceState(initialValue = listOf<Resource<Cocktail>>(Resource.Loading()), cocktailId){
        cocktailsViewModel.getById(cocktailId).collect {
            value = listOf(it)
        }
    }

    Surface(
        mCocktail = cocktail.value.first(),
        cocktailsViewModel = cocktailsViewModel
    )
}

@Composable
fun Surface(
    mCocktail: Resource<Cocktail>,
    cocktailsViewModel: CocktailsViewModel
){
    when(mCocktail){
        is Resource.Error -> Text(text = "Error")
        is Resource.Loading -> Text(text = "Loading")
        is Resource.Success -> {
            val cocktail = mCocktail.data!!
            val resource = cocktailsViewModel.getImage(cocktail.image).collectAsState()
            Layout(
                onFavoriteClicked = { cocktailsViewModel.updateFavoriteStatus(cocktail.id, cocktail.isFavorite.not()) },
                cocktail = cocktail,
                resource = resource
            )
        }
    }
}

@Composable
fun Layout(
    onFavoriteClicked: () -> Unit,
    cocktail: Cocktail,
    resource: State<Resource<Bitmap>>
) {
    val iconState = remember { mutableStateOf(cocktail.isFavorite) }
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState)
            .background(CocktailsTheme.colors.commonBackground),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.statusBarsPadding())
        Image(
            modifier = Modifier
                .width(300.dp)
                .height(300.dp)
                .alpha(min(1f, 1 - scrollState.value / 1000f)),
            resource = resource.value
        )
        Text(
            modifier = Modifier
                .absolutePadding(32.dp, 24.dp, 8.dp, 0.dp)
                .fillMaxWidth(),
            textAlign = TextAlign.Left,
            color = CocktailsTheme.colors.textColor,
            style = LocalTextStyle.current.copy(lineHeight = 20.sp),
            maxLines = 1,
            text = cocktail.name,
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold
        )
        Text(
            modifier = Modifier
                .absolutePadding(32.dp, 16.dp, 32.dp, 8.dp)
                .fillMaxWidth(),
            color = CocktailsTheme.colors.textColor,
            style = LocalTextStyle.current.copy(lineHeight = 20.sp),
            textAlign = TextAlign.Left,
            fontSize = 14.sp,
            text = cocktail.description
        )
        Text(
            modifier = Modifier
                .absolutePadding(32.dp, 24.dp, 8.dp, 0.dp)
                .fillMaxWidth(),
            color = CocktailsTheme.colors.textColor,
            textAlign = TextAlign.Left,
            maxLines = 1,
            text = "Ingredients",
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold
        )
        Text(
            modifier = Modifier
                .absolutePadding(32.dp, 16.dp, 32.dp, 8.dp)
                .fillMaxWidth(),
            style = LocalTextStyle.current.copy(lineHeight = 20.sp),
            color = CocktailsTheme.colors.textColor,
            textAlign = TextAlign.Left,
            fontSize = 14.sp,
            text = cocktail.getPreparedIngredients()
        )
        Text(
            modifier = Modifier
                .absolutePadding(32.dp, 24.dp, 8.dp, 0.dp)
                .fillMaxWidth(),
            color = CocktailsTheme.colors.textColor,
            textAlign = TextAlign.Left,
            maxLines = 1,
            text = "How to make",
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold
        )
        Text(
            modifier = Modifier
                .absolutePadding(32.dp, 16.dp, 32.dp, 8.dp)
                .fillMaxWidth(),
            color = CocktailsTheme.colors.textColor,
            textAlign = TextAlign.Left,
            style = LocalTextStyle.current.copy(lineHeight = 20.sp),
            fontSize = 14.sp,
            text = cocktail.getSteps()
        )
        Spacer(modifier = Modifier.navigationBarsPadding())
    }
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopEnd
    ) {
        Column {
            Spacer(modifier = Modifier.statusBarsPadding())
            Icon(
                modifier = Modifier
                    .padding(16.dp)
                    .clickable {
                        onFavoriteClicked.invoke()
                        iconState.value = iconState.value.not()
                    },
                tint = Color.Red,
                imageVector = if(iconState.value) Icons.Outlined.Favorite else Icons.Outlined.FavoriteBorder,
                contentDescription = "TODO Favorite"
            )
        }
    }
}