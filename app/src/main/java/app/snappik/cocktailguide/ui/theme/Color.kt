package app.snappik.cocktailguide.ui.theme

import androidx.compose.ui.graphics.Color

val SystemUI = Color(0xFFFFFFFF)
val SystemDark = Color(0xFF000000)

val BottomBarBackground = Color(0xFFFFFFFF)
val BottomBarBackgroundDark = Color(0xFF000000)
val BottomBarIconActive = Color(0xFFFC0000)
val BottomBarIconInactive = Color(0xFF757575)
val BottomBarIconActiveDark = Color(0xFFFFFFFF)
val BottomBarIconInactiveDark = Color(0xFFCA2F2F)

val TextColor = Color(0xFF000000)
val TextColorDark = Color(0xFFFFFFFF)
val CardBackground = Color(0xFFF5F3F3)
val CardBackgroundBlack = Color(0xFF242424)
val CommonBackground = Color(0xFFFFFFFF)
val CommonBackgroundDark = Color(0xFF030303)

const val AlphaNearOpaque = 0.95f
