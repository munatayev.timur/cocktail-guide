package app.snappik.cocktailguide.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import com.google.accompanist.systemuicontroller.rememberSystemUiController

private val LightColorPalette = CocktailsColors(
    cardBackground = CardBackground,
    commonBackground = CommonBackground,
    textColor = TextColor,
    bottomBackground = BottomBarBackground,
    bottomIconsColorActive = BottomBarIconActive,
    bottomIconsColorInactive = BottomBarIconInactive,
    systemUI = SystemUI,
    isDark = false
)

private val DarkColorPalette = CocktailsColors(
    cardBackground = CardBackgroundBlack,
    commonBackground = CommonBackgroundDark,
    textColor = TextColorDark,
    bottomBackground = BottomBarBackgroundDark,
    bottomIconsColorActive = BottomBarIconActiveDark,
    bottomIconsColorInactive = BottomBarIconInactiveDark,
    systemUI = SystemDark,
    isDark = true
)

@Stable
class CocktailsColors(
    cardBackground: Color,
    commonBackground: Color,
    textColor: Color,
    bottomBackground: Color,
    bottomIconsColorActive:Color,
    bottomIconsColorInactive:Color,
    systemUI: Color,
    isDark: Boolean
) {
    var cardBackground by mutableStateOf(cardBackground)
        private set
    var commonBackground by mutableStateOf(commonBackground)
        private set
    var textColor by mutableStateOf(textColor)
        private set
    var bottomBackground by mutableStateOf(bottomBackground)
        private set
    var bottomIconsColorActive by mutableStateOf(bottomIconsColorActive)
        private set
    var bottomIconsColorInactive by mutableStateOf(bottomIconsColorInactive)
        private set
    var systemUI by mutableStateOf(systemUI)
        private set
    var isDark by mutableStateOf(isDark)
        private set

    fun update(other: CocktailsColors) {
        systemUI = other.systemUI
        isDark = other.isDark
    }
}


object CocktailsTheme {
    val colors: CocktailsColors
        @Composable
        get() = LocalGuidColors.current
}

@Composable
fun CocktailsTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) DarkColorPalette else LightColorPalette

    val sysUiController = rememberSystemUiController()
    SideEffect {
        sysUiController.setSystemBarsColor(
            color = colors.systemUI.copy(alpha = AlphaNearOpaque)
        )
    }

    ProvideCocktailColors(colors) {
        MaterialTheme(
            colors = debugColors(darkTheme),
            typography = Typography,
            content = content
        )
    }
}

fun debugColors(
    darkTheme: Boolean,
    debugColor: Color = Color.Magenta
) = Colors(
    primary = debugColor,
    primaryVariant = debugColor,
    secondary = debugColor,
    secondaryVariant = debugColor,
    background = debugColor,
    surface = debugColor,
    error = debugColor,
    onPrimary = debugColor,
    onSecondary = debugColor,
    onBackground = debugColor,
    onSurface = debugColor,
    onError = debugColor,
    isLight = !darkTheme
)

@Composable
fun ProvideCocktailColors(
    colors: CocktailsColors,
    content: @Composable () -> Unit
) {
    val colorPalette = remember { colors }
    colorPalette.update(colors)
    CompositionLocalProvider(LocalGuidColors provides colorPalette, content = content)
}

private val LocalGuidColors = staticCompositionLocalOf<CocktailsColors> {
    error("No JetsnackColorPalette provided")
}