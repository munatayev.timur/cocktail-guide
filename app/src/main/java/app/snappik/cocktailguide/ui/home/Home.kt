package app.snappik.cocktailguide.ui.home

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.snappik.cocktailguide.R
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.model.Resource
import app.snappik.cocktailguide.ui.theme.CocktailsTheme
import app.snappik.cocktailguide.viewmodel.CocktailsViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter

@ExperimentalCoroutinesApi
@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun Home(
    onCocktailSelected: (Cocktail) -> Unit,
    cocktailsViewModel: CocktailsViewModel
){
    val localCocktailsSuggestions by cocktailsViewModel.randomCocktailsLocal.collectAsState()
    val randomCocktailsSuggestions by cocktailsViewModel.randomCocktailsRemote.collectAsState()
    val recentlyOpened by cocktailsViewModel.recentlyOpened.collectAsState()
    val scrollState = rememberScrollState()

    LaunchedEffect(randomCocktailsSuggestions) {
        snapshotFlow { randomCocktailsSuggestions.all { it is Resource.Loading } }
            .distinctUntilChanged()
            .filter { it }
            .collect {
                cocktailsViewModel.loadRandom()
            }
    }

    Box(modifier = Modifier
        .background(CocktailsTheme.colors.bottomBackground)
        .fillMaxSize()
    ) {

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(CocktailsTheme.colors.bottomBackground)
                .verticalScroll(scrollState)
        ) {

            Text(
                modifier = Modifier.absolutePadding(0.dp, 48.dp, 0.dp, 0.dp),
                text = stringResource(R.string.app_name),
                color = CocktailsTheme.colors.textColor,
                fontSize = 36.sp
            )

            ListCocktail(
                text = stringResource(R.string.home_loaded),
                data = localCocktailsSuggestions,
                onCocktailSelected = onCocktailSelected,
                cocktailsViewModel = cocktailsViewModel
            )

            ListCocktail(
                text = stringResource(R.string.home_recent),
                data = recentlyOpened,
                onCocktailSelected = onCocktailSelected,
                cocktailsViewModel = cocktailsViewModel
            )

            Random(
                data = randomCocktailsSuggestions,
                onCocktailSelected = onCocktailSelected,
                cocktailsViewModel = cocktailsViewModel
            )
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun ListCocktail(
    text: String,
    data: Resource<Set<Cocktail>>,
    onCocktailSelected: (Cocktail) -> Unit,
    cocktailsViewModel: CocktailsViewModel
){
    if(data.data?.size ?: 1 > 0){
        Column(
            modifier = Modifier.absolutePadding(0.dp, 16.dp, 0.dp, 0.dp),
        ) {
            Text(
                text = text,
                color = CocktailsTheme.colors.textColor,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.absolutePadding(8.dp, 8.dp, 8.dp, 0.dp),
                maxLines = 1,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )
            LazyRow {
                items(
                    items = data.data?.toList() ?: emptyList(),
                    key = { cocktail -> cocktail.id }
                ) { cocktail ->
                    CocktailCell(
                        onCocktailSelected,
                        cocktailsViewModel,
                        cocktail
                    )
                }
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun Random(
    data: List<Resource<Cocktail>>,
    onCocktailSelected: (Cocktail) -> Unit,
    cocktailsViewModel: CocktailsViewModel
){
    Column {
        Text(
            text = stringResource(R.string.home_random),
            overflow = TextOverflow.Ellipsis,
            color = CocktailsTheme.colors.textColor,
            modifier = Modifier.absolutePadding(8.dp, 8.dp, 8.dp, 0.dp),
            maxLines = 1,
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold
        )
        LazyRow {
            items(
                items = data,
                key = { cocktail -> cocktail.data?.id ?: -1 }
            ) { cocktail ->
                CocktailCell(
                    onCocktailSelected,
                    cocktailsViewModel,
                    cocktail.data
                )
            }
        }
    }
}