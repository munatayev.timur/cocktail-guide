package app.snappik.cocktailguide.ui.search

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.ui.components.CocktailsDivider
import app.snappik.cocktailguide.ui.components.SearchBar
import app.snappik.cocktailguide.ui.theme.CocktailsTheme
import app.snappik.cocktailguide.viewmodel.CocktailsViewModel
import com.google.accompanist.insets.statusBarsPadding

@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun Search(
    isFavorites: Boolean,
    onCocktailSelected: (Cocktail) -> Unit,
    cocktailsViewModel: CocktailsViewModel
) {
    Surface(modifier = Modifier.absolutePadding(0.dp, 0.dp, 0.dp, 48.dp)
        .background(CocktailsTheme.colors.commonBackground)) {
        Column(
            modifier = Modifier.background(CocktailsTheme.colors.commonBackground)
        ) {
            val searchQueryFilter = remember { mutableStateOf("") }

            if(isFavorites.not()) {
                LaunchedEffect(searchQueryFilter.value) {
                    if (searchQueryFilter.value.isEmpty()) {
                        cocktailsViewModel.loadMore()
                    } else {
                        cocktailsViewModel.search(searchQueryFilter.value)
                    }
                }
            }

            Spacer(modifier = Modifier.statusBarsPadding())
            SearchBar(
               onFilterChange = { query -> searchQueryFilter.value = query }
            )
            CocktailsDivider()
            CocktailsList(
                searchQueryFilter,
                isFavorites,
                onCocktailSelected,
                cocktailsViewModel
            )
        }
    }
}