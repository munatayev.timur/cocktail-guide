package app.snappik.cocktailguide.ui.home

import android.graphics.Bitmap
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.model.Resource
import app.snappik.cocktailguide.ui.components.Image
import app.snappik.cocktailguide.ui.theme.CocktailsTheme
import app.snappik.cocktailguide.viewmodel.CocktailsViewModel
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalMaterialApi
@Composable
fun CocktailCell(
    onCocktailSelected: (Cocktail) -> Unit,
    cocktailsViewModel: CocktailsViewModel,
    cocktail: Cocktail?
) {
    val resource = cocktailsViewModel.getImage(cocktail?.image).collectAsState()
    CellLayout(
        onCocktailSelected,
        cocktail,
        resource
    )
}

@ExperimentalMaterialApi
@Composable
fun CellLayout(
    onCocktailSelected: (Cocktail) -> Unit,
    mCocktail: Cocktail?,
    resource: State<Resource<Bitmap>>
){

    val alpha = remember { Animatable(0F) }
    LaunchedEffect(true) {
        alpha.animateTo(
            targetValue = 1F,
            animationSpec = tween(durationMillis = 1000, easing = FastOutSlowInEasing)
        )
    }

    mCocktail?.let { cocktail ->
        Card(
            modifier = Modifier
                .padding(16.dp)
                .width(150.dp)
                .height(150.dp)
                .alpha(alpha.value),
            onClick = { onCocktailSelected(cocktail) }
        ) {
            Column(
                modifier = Modifier.background(CocktailsTheme.colors.cardBackground),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    modifier = Modifier
                        .width(100.dp)
                        .height(100.dp)
                        .padding(8.dp),
                    resource = resource.value
                )
                Text(
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.absolutePadding(8.dp, 4.dp, 8.dp, 8.dp),
                    maxLines = 1,
                    color = CocktailsTheme.colors.textColor,
                    text = cocktail.name,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
            }
        }
    } ?: CircularProgressIndicator(
        modifier = Modifier
            .width(150.dp)
            .height(150.dp)
            .padding(16.dp)
    )
}

@ExperimentalMaterialApi
@Preview()
@Composable
private fun SearchRowPreview() {
    CocktailsTheme {
        CellLayout(
            onCocktailSelected = {},
            mCocktail = Cocktail(name = "Test Name", description = "Test descript"),
            resource = MutableStateFlow<Resource<Bitmap>>(Resource.Loading()).collectAsState()
        )
    }
}