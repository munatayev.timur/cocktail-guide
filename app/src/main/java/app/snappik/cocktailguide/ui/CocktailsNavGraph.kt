package app.snappik.cocktailguide.ui

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navigation
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.ui.MainDestinations.COCKTAIL_ID_KEY
import app.snappik.cocktailguide.ui.MainDestinations.DETAILS_ROUTE
import app.snappik.cocktailguide.ui.details.Details
import app.snappik.cocktailguide.ui.home.Home
import app.snappik.cocktailguide.ui.search.Search
import app.snappik.cocktailguide.viewmodel.CocktailsViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

object MainDestinations {
    const val HOME_ROUTE = "home"
    const val DETAILS_ROUTE = "details"
    const val COCKTAIL_ID_KEY = "cocktailId"
}

@ExperimentalCoroutinesApi
@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun CocktailsNavGraph(
    navController: NavHostController = rememberNavController(),
    startDestination: String = MainDestinations.HOME_ROUTE,
    cocktailsViewModel: CocktailsViewModel = hiltViewModel()
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
    ) {
        navigation(
            route = MainDestinations.HOME_ROUTE,
            startDestination = NavigationSections.HOME.route
        ) {

            val onCocktailSelected = { cocktail: Cocktail, from: NavBackStackEntry ->
                if (from.lifecycleIsResumed()) {
                    navController.navigate("$DETAILS_ROUTE/${cocktail.id}")
                }
            }

            composable(NavigationSections.HOME.route) { from ->
                Home(onCocktailSelected = { cocktail ->  onCocktailSelected(cocktail, from) }, cocktailsViewModel)
            }
            composable(NavigationSections.SEARCH.route) { from ->
                Search(false, onCocktailSelected = { cocktail ->  onCocktailSelected(cocktail, from) }, cocktailsViewModel)
            }
            composable(NavigationSections.FAVORITES.route) { from ->
                Search(true, onCocktailSelected = { cocktail ->  onCocktailSelected(cocktail, from) }, cocktailsViewModel)
            }
            composable(
                route = "${DETAILS_ROUTE}/{$COCKTAIL_ID_KEY}",
                arguments = listOf(navArgument(COCKTAIL_ID_KEY) { type = NavType.LongType })
            ) { backStackEntry ->
                val arguments = requireNotNull(backStackEntry.arguments)
                val cocktailId = arguments.getLong(COCKTAIL_ID_KEY)
                Details(
                    cocktailId = cocktailId,
                    cocktailsViewModel = cocktailsViewModel
                )
            }
        }
    }
}

private fun NavBackStackEntry.lifecycleIsResumed() = this.lifecycle.currentState == Lifecycle.State.RESUMED
