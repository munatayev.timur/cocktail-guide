package app.snappik.cocktailguide.ui.search

import android.graphics.Bitmap
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.model.Resource
import app.snappik.cocktailguide.ui.components.Image
import app.snappik.cocktailguide.ui.theme.CocktailsTheme
import app.snappik.cocktailguide.viewmodel.CocktailsViewModel
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalMaterialApi
@Composable
fun CocktailRow(
    onCocktailSelected: (Cocktail) -> Unit,
    onFavoriteClicked: () -> Unit,
    cocktailsViewModel: CocktailsViewModel,
    cocktail: Cocktail
) {
    val resource = cocktailsViewModel.getImage(cocktail.image).collectAsState()
    RowLayout(
        onCocktailSelected,
        onFavoriteClicked,
        cocktail,
        resource
    )
}

@ExperimentalMaterialApi
@Composable
fun RowLayout(
    onCocktailSelected: (Cocktail) -> Unit,
    onFavoriteClicked: () -> Unit,
    cocktail: Cocktail,
    resource: State<Resource<Bitmap>>
){

    val alpha = remember { Animatable(0F) }
    LaunchedEffect(true) {
        alpha.animateTo(
            targetValue = 1F,
            animationSpec = tween(durationMillis = 1000, easing = FastOutSlowInEasing)
        )
    }

    Card(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
            .alpha(alpha.value),
        onClick = { onCocktailSelected(cocktail) }
    ) {

        val iconState = remember { mutableStateOf(cocktail.isFavorite) }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.background(CocktailsTheme.colors.cardBackground)
        ) {
            Image(
                modifier = Modifier
                    .width(100.dp)
                    .height(100.dp)
                    .padding(8.dp),
                resource = resource.value
            )
            Column(
                modifier = Modifier.background(CocktailsTheme.colors.cardBackground)
            ) {
                Text(
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.absolutePadding(8.dp, 4.dp, 8.dp, 0.dp),
                    maxLines = 1,
                    color = CocktailsTheme.colors.textColor,
                    text = cocktail.name,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.absolutePadding(8.dp, 4.dp, 8.dp, 8.dp),
                    color = CocktailsTheme.colors.textColor,
                    maxLines = 3,
                    fontSize = 14.sp,
                    text = cocktail.description,
                    style = LocalTextStyle.current.copy(lineHeight = 20.sp)
                )
            }
        }
        Box(
            contentAlignment = Alignment.TopEnd
        ) {
            Icon(
                modifier = Modifier
                    .padding(8.dp)
                    .clickable {
                        onFavoriteClicked.invoke()
                        iconState.value = iconState.value.not()
                    },
                tint = Color.Red,
                imageVector = if(iconState.value) Icons.Outlined.Favorite else Icons.Outlined.FavoriteBorder,
                contentDescription = "TODO Favorite"
            )
        }
    }
}

@ExperimentalMaterialApi
@Preview
@Composable
private fun SearchRowPreview() {
    CocktailsTheme {
        RowLayout(
            onCocktailSelected = {},
            onFavoriteClicked = {},
            cocktail = Cocktail(name = "Test Name", description = "Test descript"),
            resource = MutableStateFlow<Resource<Bitmap>>(Resource.Loading()).collectAsState()
        )
    }
}