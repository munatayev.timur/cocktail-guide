package app.snappik.cocktailguide.ui

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.compose.rememberNavController
import app.snappik.cocktailguide.ui.components.bottom.CocktailBottomBar
import app.snappik.cocktailguide.ui.theme.CocktailsTheme
import com.google.accompanist.insets.ProvideWindowInsets
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun CocktailApp() {
    ProvideWindowInsets {
        CocktailsTheme {
            val tabs = remember { NavigationSections.values() }
            val navController = rememberNavController()
            Scaffold(
                bottomBar = { CocktailBottomBar(navController = navController, tabs = tabs) }
            ) {
                CocktailsNavGraph(
                    navController = navController
                )
            }
        }
    }
}

/*
    FIXME
    - pagination updates the whole composition (list)
    - during fetching next part(pagination) show show loader
    - loaded random data at Home screen in offline will have infinite loaders (add time out)
 */