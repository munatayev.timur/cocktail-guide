package app.snappik.cocktailguide.ui.components

import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.unit.dp
import app.snappik.cocktailguide.ui.theme.CocktailsTheme

@ExperimentalAnimationApi
@Composable
fun SearchBar(
    onFilterChange: (filter: String) -> Unit,
) {
    Surface(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier
            .fillMaxWidth()
            .height(56.dp)
            .padding(horizontal = 24.dp, vertical = 8.dp)
            .background(CocktailsTheme.colors.commonBackground)
    ) {
        Box(
            modifier = Modifier.fillMaxSize()
                        .background(CocktailsTheme.colors.commonBackground)
        ) {
            val searchQuery = remember { mutableStateOf("") }
            val searchFocus = remember { mutableStateOf(false) }
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxSize()
                    .wrapContentHeight()
            ) {
                BasicTextField(
                    value = searchQuery.value,
                    onValueChange = {
                        searchQuery.value = it
                        onFilterChange.invoke(it)
                    },
                    modifier = Modifier
                        .weight(1f)
                        .background(CocktailsTheme.colors.commonBackground)
                        .onFocusChanged { focusState ->
                            searchFocus.value = focusState.isFocused
                        }
                )
            }
            SearchHint(
                searchFocus.value,
                searchQuery.value.isEmpty()
            )
        }
    }
}

@ExperimentalAnimationApi
@Composable
private fun SearchHint(
    isFocused: Boolean,
    isVisible: Boolean
) {
    AnimatedVisibility(
        visible = isVisible && isFocused.not(),
        enter = fadeIn(),
        exit = fadeOut(),
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxSize()
                .wrapContentSize()
        ) {
            Icon(
                imageVector = Icons.Outlined.Search,
                contentDescription = "TODO",
                tint = CocktailsTheme.colors.textColor
            )
            Spacer(Modifier.width(8.dp))
            Text(
                text = "Search",
                color = CocktailsTheme.colors.textColor,
            )
        }
    }
}

/*
    Search logic
    1. filter already stored locally items by filter
    2. Get data from remote by filter
    3. if remote data is bigger than local, save to database and return
    4. If remote data same as local return local
    5. Also remove local if offline or network problems
 */