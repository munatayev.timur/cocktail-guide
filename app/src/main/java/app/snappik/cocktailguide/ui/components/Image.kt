package app.snappik.cocktailguide.ui.components

import android.graphics.Bitmap
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import app.snappik.cocktailguide.model.Resource

@Composable
fun Image(
    modifier: Modifier,
    resource: Resource<Bitmap>
){
    when(resource){
        is Resource.Loading -> {
            CircularProgressIndicator(
                modifier = modifier
            )
        }
        is Resource.Error -> {
            Text(
                modifier = modifier,
                text = "Error"
            )
        }
        is Resource.Success -> {
            androidx.compose.foundation.Image(
                modifier = modifier,
                bitmap = resource.data!!.asImageBitmap(),
                contentScale = ContentScale.Crop,
                contentDescription = ""
            )
        }
    }
}