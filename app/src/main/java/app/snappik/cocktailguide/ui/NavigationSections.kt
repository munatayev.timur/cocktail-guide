package app.snappik.cocktailguide.ui

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.ui.graphics.vector.ImageVector
import app.snappik.cocktailguide.R

enum class NavigationSections(
    @StringRes val title: Int,
    val icon: ImageVector,
    val route: String
) {
    HOME(R.string.nav_home, Icons.Outlined.Home, "navigation/home"),
    SEARCH(R.string.nav_search, Icons.Outlined.Search, "navigation/search"),
    FAVORITES(R.string.nav_favorites, Icons.Outlined.Favorite, "navigation/favorite")
}