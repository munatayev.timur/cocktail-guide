package app.snappik.cocktailguide.ui.components.bottom

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.SpringSpec
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.height
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.util.lerp
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavGraph
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import app.snappik.cocktailguide.ui.theme.CocktailsTheme
import app.snappik.cocktailguide.ui.NavigationSections
import com.google.accompanist.insets.navigationBarsPadding

@Composable
fun CocktailBottomBar(
    navController: NavController,
    tabs: Array<NavigationSections>
) {

    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route
    val sections = remember { NavigationSections.values() }
    val routes = remember { sections.map { it.route } }

    if (currentRoute in routes) {
        val currentSection = sections.first { it.route == currentRoute }
        Surface {
            val springSpec = SpringSpec<Float>(
                stiffness = 800f,
                dampingRatio = 0.8f
            )
            CocktailsBottomNavLayout(
                selectedIndex = currentSection.ordinal,
                itemCount = routes.size,
                modifier = Modifier.navigationBarsPadding(start = false, end = false)
            ) {
                tabs.forEach { section ->
                    val selected = section == currentSection
                    val tint by animateColorAsState(
                        if (selected) {
                            CocktailsTheme.colors.bottomIconsColorActive
                        } else {
                            CocktailsTheme.colors.bottomIconsColorInactive
                        }
                    )

                    CocktailBottomItem(
                        icon = {
                            Icon(
                                imageVector = section.icon,
                                tint = tint,
                                contentDescription = null
                            )
                        },
                        text = {
                            Text(
                                text = stringResource(section.title),
                                color = tint,
                                style = MaterialTheme.typography.button,
                                maxLines = 1
                            )
                        },
                        selected = selected,
                        onSelected = {
                            if (section.route != currentRoute) {
                                navController.navigate(section.route) {
                                    launchSingleTop = true
                                    restoreState = true
                                    popUpTo(findStartDestination(navController.graph).id) {
                                        saveState = true
                                    }
                                }
                            }
                        },
                        animSpec = springSpec
                    )
                }
            }
        }
    }
}

@Composable
private fun CocktailsBottomNavLayout(
    selectedIndex: Int,
    itemCount: Int,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    val selectionFractions = remember(itemCount) {
        List(itemCount) { i ->
            Animatable(if (i == selectedIndex) 1f else 0f)
        }
    }
    Layout(
        modifier = modifier
            .height(48.dp)
            .background(Color.White),
        content = content
    ) { measurable, constraints ->
        val itemWidth = constraints.maxWidth / itemCount
        val itemPlaceable = measurable
            .mapIndexed { index, mes ->
                val width = lerp(itemWidth, itemWidth, selectionFractions[index].value)
                mes.measure(
                    constraints.copy(
                        minWidth = width,
                        maxWidth = width
                    )
                )
            }
        layout(
            width = constraints.maxWidth,
            height = itemPlaceable.maxByOrNull { it.height }?.height ?: 0
        ) {
            var x = 0
            itemPlaceable.forEach { placeable ->
                placeable.placeRelative(x = x, y = 0)
                x += placeable.width
            }
        }
    }
}

private val NavGraph.startDestination: NavDestination?
    get() = findNode(startDestinationId)

private tailrec fun findStartDestination(graph: NavDestination): NavDestination {
    return if (graph is NavGraph) findStartDestination(graph.startDestination!!) else graph
}

@Preview
@Composable
private fun BottomNavBarPreview() {
    CocktailsTheme {
        CocktailBottomBar(
            navController = rememberNavController(),
            tabs = NavigationSections.values()
        )
    }
}