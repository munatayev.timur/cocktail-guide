package app.snappik.cocktailguide.ui.search

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.snappik.cocktailguide.model.Cocktail
import app.snappik.cocktailguide.model.Resource
import app.snappik.cocktailguide.ui.theme.CocktailsTheme
import app.snappik.cocktailguide.viewmodel.CocktailsViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter

@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun CocktailsList(
    filter: State<String>,
    isFavorites: Boolean,
    onCocktailSelected: (Cocktail) -> Unit,
    cocktailsViewModel: CocktailsViewModel
) {
    val viewState by if(isFavorites) {
        cocktailsViewModel.favorites.collectAsState()
    } else cocktailsViewModel.cocktails.collectAsState()

    val listState = rememberLazyListState()

    if(isFavorites.not()) {
        LaunchedEffect(listState) {
            /*
            Reading the state directly in composition is useful when you need to update other UI composable,
            but there are also scenarios where the event does not need to be handled in the same composition.
            A common example of this is sending an analytics event once the user has scrolled past a certain point.
            */
            snapshotFlow { listState.isScrolledToTheEnd() && filter.value.isEmpty() }
                .distinctUntilChanged()
                .filter { it }
                .collect {
                    cocktailsViewModel.loadMore()
                }
        }
    }

    CocktailListContent(
        filter = filter,
        data = viewState,
        onCocktailSelected = onCocktailSelected,
        cocktailsViewModel = cocktailsViewModel,
        listState = listState
    )
}

@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun CocktailListContent(
    filter: State<String>,
    data: Resource<Set<Cocktail>>,
    onCocktailSelected: (Cocktail) -> Unit,
    cocktailsViewModel: CocktailsViewModel,
    listState: LazyListState
){
    when{
        data is Resource.Error -> {
            Column(
                modifier = Modifier.fillMaxSize().background(CocktailsTheme.colors.commonBackground),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = data.message ?: "Unknown error",
                    modifier = Modifier.absolutePadding(8.dp, 16.dp, 8.dp, 8.dp),
                    fontSize = 18.sp
                )
            }
        }
        data is Resource.Loading && data.data.isNullOrEmpty() -> {
            Column(
                modifier = Modifier.fillMaxSize().background(CocktailsTheme.colors.commonBackground),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Loading...",
                    modifier = Modifier.absolutePadding(8.dp, 16.dp, 8.dp, 8.dp),
                    fontSize = 18.sp
                )
            }
        }
        else -> LazyColumn(
            modifier = Modifier.fillMaxSize().background(CocktailsTheme.colors.commonBackground),
            state = listState
        ) {
            items(
                items = data.data?.filter { it.name.contains(filter.value) } ?: emptyList(),
                key = { cocktail -> cocktail.id }
            ) { cocktail ->
                CocktailRow(
                    onCocktailSelected,
                    onFavoriteClicked = { cocktailsViewModel.updateFavoriteStatus(cocktail.id, cocktail.isFavorite.not()) },
                    cocktailsViewModel,
                    cocktail
                )
            }
        }
    }
}

fun LazyListState.isScrolledToTheEnd() = layoutInfo.visibleItemsInfo.lastOrNull()?.index == layoutInfo.totalItemsCount - 1

/*
* When parameter changes in Compose, the whole compose object is recreated
* Hoist state pattern is when remember state moved up and state parameter passed as variable of function
* */