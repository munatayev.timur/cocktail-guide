package app.snappik.cocktailguide

import android.app.Application
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Logger
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application(){

    override fun onCreate() {
        super.onCreate()
        initFirebase()
    }

    private fun initFirebase(){
        FirebaseDatabase.getInstance().setLogLevel(Logger.Level.DEBUG)
    }
}