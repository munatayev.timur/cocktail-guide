package app.snappik.cocktailguide.storage

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import app.snappik.cocktailguide.model.Resource
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class LocalStorage @Inject constructor(context: Context) {

    private val imagesFolder = File(context.filesDir.absolutePath + IMAGES_FOLDER)

    init {
        if(imagesFolder.exists().not()){
            imagesFolder.mkdir()
        }
    }

    suspend fun getImage(name: String) = flow {
        val file = File("${imagesFolder.absolutePath}/${name}")
        //1.Check if the file exists
        file.takeIf { it.exists() }
            //2.If file exist decode it and return bitmap
            ?.let { BitmapFactory.decodeFile(it.absolutePath) }
            ?.also { emit(it) }
            //2.If file doesn't exist throw exception to fetch from remote storage
            ?: throw Exception()
    }

    suspend fun fetch(remoteFile: StorageReference) = suspendCoroutine<Resource<Bitmap>> { cont ->
        val localFile = File("${imagesFolder.absolutePath}/${remoteFile.name}")
        //Save remote file to local file
        remoteFile
            .getFile(localFile)
            .addOnFailureListener { cont.resume(Resource.Error(it.message ?: "Can not fetch image from storage")) }
            .addOnSuccessListener {
                //If file saved locally decode file into bitmap and return
                CoroutineScope(Dispatchers.IO).launch {
                    BitmapFactory.decodeFile(localFile.absolutePath)?.let { bitmap ->
                        cont.resume(Resource.Success(bitmap))
                    } ?: cont.resume(Resource.Error("Can not decode bitmap"))
                }
            }
    }

    companion object {
        private const val IMAGES_FOLDER = "/images"
    }
}