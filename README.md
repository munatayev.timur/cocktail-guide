# Cocktail Guid

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Application created with Jetpack Compose

## Description

Cocktail Guid application created specially to test Jetpack Compose and architecture connected to it. 

### Bulet features

- simple
- light weight
- offline storing
- list pagination
- elastic search
- search pagination
- favorites
- different sorting sugesstions

## How use
1. **Home menu**. Different cocktails sugestions (from localy stored items, random from local and remote, recently seen)
2. **Search menu**. List of cocktails in alphabetic order. Dynamicaly fetches items (pagination). Posibility to search between local and remote (all searched items from remote automatically stored localy)
3. **Favorites**. Posibility to mark cocktails as favorite.


### What used

- Jetpack Compose
- MVVM architecture
- Kotlin
- Kotlin DSL
- Kotlin coroutines
- Room database
- Kotlin Flow
- Firebase Realtime Database

### Keywords

cocktails, bar, jetpack compose, kotlin, mvvm

## Download

<a href='https://play.google.com/store/apps/details?id=app.snappik.cocktailguide'><img alt='Get it on Google Play' width="300" height="120" src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>

## Authors

* **Munatayev Timur**

## License

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0
